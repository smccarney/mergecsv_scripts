import requests
import json
import pandas as pd
import numpy as np
import re
import boto3
from botocore.client import Config
from functools import reduce
import time

##############################################################################
# The following to functions and function calls retrieve project details,
# equivalent to the lamba_function in AWS
slug = "Imvigor210"
aws_access_key, aws_secret_key, region, csv1_url, csv2_url, project_name = aws_cred(slug)
filename_urls = [csv2_url, csv1_url]
bucket_name = re.split("/", csv1_url)[2]
filenames = [re.split("/", csv1_url)[-1], re.split("/", csv2_url)[-1]]
print(filename_urls)
print(bucket_name)

df = merge_csv(filename_urls).reset_index().rename(columns={"index": "unique_id"})
parquet = df.to_parquet("{}.parquet".format(slug))
s3_client = boto3.client('s3', aws_access_key_id=aws_access_key,
                        aws_secret_access_key=aws_secret_key,
                        config=Config(signature_version='s3v4'))
#s3_client.upload_file(f"{slug}.parquet", bucket_name, f"{slug}.parquet")

def project_get(slug):
    url = "https://postgrest-api.indraapp.com/projects?project_slug=eq.{}".format(slug)
    response = requests.get(url)
    print(response.status_code)
    j = response.json()
    aws_series = pd.Series(j[0])
    return aws_series

def aws_cred(slug):
    aws_series = project_get(slug)
    aws_access_key = aws_series["project_data_source_username"]
    aws_secret_key = aws_series["project_data_source_password"]
    region = aws_series["project_data_source_certificate"]
    csv1_url = aws_series["project_data_source_url"]
    csv2_url = aws_series["project_data_source_url2"]
    project_name = aws_series["project_name"]
    return aws_access_key, aws_secret_key, region, csv1_url, csv2_url, project_name
#############################################################################

# Check where unique_id is and detect whether it is a column or row
def scan_index_columns(dataframes):
    """
    This function will scan the rows and the columns of a list of dataframes to
    determine where the unique_id label is in each dataframe. If the unique_id is
    in a column header it will not transpose the dataframe, however if the
    unique_id is in the index/ a row name, the dataframe will be transposed before
    merging. This function returns a list of bools ordering the transposition or
    not of the list of dataframes, and a list of column/row ids that indicate on
    which row/column the dataframes should be merged. This is currently only
    supported for a list containing two dataframes.
    """
    list_of_uniqueids, transpose_bools, index_col = ([] for [] in range(3))
    for df in dataframes:
        if "unique_id" in df.columns and "unique_id" in df.index:
            raise ValueError("Unique_id appears in both the row and column")
        elif "unique_id" in df.columns:
            print("unique_id in column")
            transpose_bools.append(False)
            list_of_uniqueids.append([df.unique_id.tolist()])
            index_col.append("unique_id")
        elif "unique_id" in df.index:
            print("unique_id in index")
            transpose_bools.append(True)
            list_of_uniqueids.append([df.loc["unique_id"].tolist()])
            index_col.append("unique_id")
        else:
            print("unique_id not found... checking cell 1")
            df.index.name = None
            transpose_bools.append(None)
            column_names = (df.columns.tolist())
            index_names = (df.index.tolist())
            list_of_uniqueids.append([column_names, index_names])
            index_col.append(0)

    if len(list_of_uniqueids) != 2:
        print(len(list_of_uniqueids))
        raise ValueError("Currently only support for 2 csv files")

    # For more csvs, generalise to python lambda/reduce function
    intersection_lengths = []
    for csv1_list in list_of_uniqueids[0]:
        for csv2_list in list_of_uniqueids[1]:
            intersection = set(csv1_list).intersection(csv2_list)
            intersection_lengths.append(len(intersection))

    # Also raise an error if the intersection does not match the length of the column/row?
    # If all the intersection lengths are 0, throw an error
    print(intersection_lengths)
    if len(np.nonzero(intersection_lengths)[0]) == 0:
        raise ValueError("No matching unique_id columns found")

    # argmax gives the index of the max value in the list (if two maxima, gives index of first)
    if transpose_bools == [None, None]:
        if np.argmax(intersection_lengths) == 0:
            transpose_bools = [True, True]
        if np.argmax(intersection_lengths) == 1:
            transpose_bools = [True, False]
        if np.argmax(intersection_lengths) == 2:
            transpose_bools = [False, True]
        if np.argmax(intersection_lengths) == 3:
            transpose_bools = [False, False]
    elif transpose_bools == [False, None]:
        if np.argmax(intersection_lengths) == 0:
            transpose_bools = [False, True]
        if np.argmax(intersection_lengths) == 1:
            transpose_bools = [False, False]
    elif transpose_bools == [True, None]:
        if np.argmax(intersection_lengths) == 0:
            transpose_bools = [True, True]
        if np.argmax(intersection_lengths) == 1:
            transpose_bools = [True, False]
    elif transpose_bools == [None, False]:
        if np.argmax(intersection_lengths) == 0:
            transpose_bools = [True, False]
        if np.argmax(intersection_lengths) == 1:
            transpose_bools = [False, False]
    elif transpose_bools == [None, True]:
        if np.argmax(intersection_lengths) == 0:
            transpose_bools = [True, True]
        if np.argmax(intersection_lengths) == 1:
            transpose_bools = [False, True]

    return transpose_bools, index_col

def merge_csv(filename_urls, commaSep=True, dtype=None, merge=True):
    # Define additional values for pandsa to recognise as NAs
    na_values = ['na', 'Na', 'nA', 'NaT', 'NaD']
    # Scan dataframes to determine the index to join on, and whether to transpose
    scan_dataframes = []
    for file in filename_urls:
        scan_dataframes.append(pd.read_csv(file, index_col=0))
    transpose, index_col = scan_index_columns(scan_dataframes)

    dataframes = []
    for file in range(len(filename_urls)):
        # Detect delimeter if csv is not comma separated
        if commaSep == False:
            file = open(filename_urls[file])
            sniffer = csv.Sniffer()
            sep = sniffer.sniff(file.read()).delimiter
        else:
            sep = ","
        # Read in csvs and make unique_id the index
        try:
            df = pd.read_csv(filename_urls[file], index_col=index_col[file],
                            sep=sep, dtype=dtype, na_values=na_values, parse_dates=True)
        except:
            df = pd.read_csv(filename_urls[file], index_col=index_col[file],
                            engine='python', encoding='latin1', dtype=dtype,
                            na_values=na_values, parse_dates=True)
        # Transpose the dataframes as required
        if transpose[file] == True:
            df = df.T
        dataframes.append(df)
    # Merge dataframes recursively
    if merge:
        df = reduce(lambda  left,right: pd.merge(right,
                                                 left,
                                                 left_on=True,
                                                 right_index=True),
                                        dataframes)
    # Try converting object dtypes to datetime
    for col in df.columns:
        if df[col].dtype == 'object':
            try:
                df[col] = pd.to_datetime(df[col])
            except ValueError:
                pass
    # Change object dtypes to categoricals
    df = df.astype(df.dtypes.where(df.dtypes == "object")
                            .dropna()
                            .replace("object", "category"))

    return df
